package com.piserve.geejo.myservicekart;

/**
 * Created by Droid Space on 6/1/2015.
 */

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {

    public static final String HOME_PAGE_NUMBER = "1";
    private static MyApplication sInstance;


    public static MyApplication getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

}
