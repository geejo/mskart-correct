package com.piserve.geejo.pojo;

/**
 * Created by Droid Space on 6/1/2015.
 */
public class Service {
    private long id;
    private String name;
    private String organizationName;
    private int rating;
    private long price;
    private String urlThumbnail;

    public Service() {

    }

    public Service(long id,
                   String name,
                   String organizationName,
                   long price,
                   int rating,
                   String urlThumbnail) {
        this.id = id;
        this.name = name;
        this.organizationName = organizationName;
        this.rating = rating;
//        this.price = price;
        this.urlThumbnail = urlThumbnail;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    @Override
    public String toString() {
        return "ID: " + id +
                "Name " + name +
//                "Price "+price+
                "OrganizationName " + organizationName +
                "Rating " + rating +
                "urlThumbnail " + urlThumbnail +
                "\n";
    }
}

