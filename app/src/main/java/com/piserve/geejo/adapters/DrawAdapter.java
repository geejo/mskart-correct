package com.piserve.geejo.adapters;

/**
 * Created by Droid Space on 6/1/2015.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import com.piserve.geejo.myservicekart.R;
import com.piserve.geejo.pojo.Information;

public class DrawAdapter extends RecyclerView.Adapter<DrawAdapter.MyViewHolder> {
    List<Information> mData = Collections.emptyList();
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public DrawAdapter(Context context, List<Information> data) {
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    public void delete(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.custom_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Information current = mData.get(position);
        holder.title.setText(current.title);
        holder.icon.setImageResource(current.iconId);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.listText);
            icon = (ImageView) itemView.findViewById(R.id.listIcon);
        }
    }
}

