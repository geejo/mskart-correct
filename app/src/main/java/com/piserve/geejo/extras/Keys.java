package com.piserve.geejo.extras;

/**
 * Created by Droid Space on 6/1/2015.
 */
public interface Keys {
    public interface EndpointBoxOffice{
        public static final String KEY_ID = "id";
        public static final String KEY_NAME = "name";
        public static final String KEY_PRICE = "price";
        public static final String KEY_ORGANIZATION_NAME = "organizationName";
        public static final String KEY_RATING = "rating";
        public static final String KEY_IMAGE_THUMBNAIL = "smallPicture";
        public static final String KEY_POSTED_USER = "postedUser";
        public static final String KEY_PROFILE = "profile";

    }
}
