package com.piserve.geejo.extras;

/**
 * Created by Droid Space on 6/1/2015.
 */
import android.os.Build;

public class Util {
    public static boolean isLollipopOrGreater() {
        return Build.VERSION.SDK_INT >= 21 ? true : false;
    }
    public static boolean isJellyBeanOrGreater(){
        return Build.VERSION.SDK_INT>=16?true:false;
    }
}
